## Kernels

1.) Netbsd 9.1 i386 

2.) FreeBSD 13.0 i386 

````
md5 /mnt/netbsd /boot/loader /boot/kernel/kernel 
MD5 (/mnt/netbsd) = 3ced33bdc659ce792889becb66ee57a1
MD5 (/boot/loader) = fbb5bbb66645a0458a36932202f2b9d3
MD5 (/boot/kernel/kernel) = d6cb07de6ba3bdfd98115654838f2a85
````

# Content



````
netbsd 1.0 kernel 
file /root/netbsd1
/root/netbsd1: a.out NetBSD/i386 demand paged executable not stripped
````


````
netbsd 3.1.1. gives : 
qemu-system-i386: Error loading uncompressed kernel without PVH ELF Note
/mnt/netbsd: ELF 32-bit LSB executable, Intel 80386, version 1 (SYSV), statically linked, not stripped

````


````
5.1.1. gives: 
/mnt/netbsd: ELF 32-bit LSB executable, Intel 80386, version 1 (SYSV), statically linked, for NetBSD 5.1, with debug_info, not stripped
````
